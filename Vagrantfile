# -*- mode: ruby -*-
# vi: set ft=ruby :
require 'yaml'

current_dir = File.join(File.dirname(__FILE__))
if File.file?("#{current_dir}/config/config.yaml")
  configsettings     = YAML.load_file("#{current_dir}/config/config.yaml")
else
  raise "Конфигурационный файл отсутствует"
end

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/focal64"
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.synced_folder "./www", "/var/www/html", disabled: true 
  
  config.vm.define "nginx-1" do |nginxn1|
    nginxn1.vm.hostname = "nginxn1"
    nginxn1.vm.network "private_network", ip: "172.16.0.10"
    nginxn1.vm.network "forwarded_port", guest: 22, host: 2221, id: "ssh"
   
    
    nginxn1.vm.provider "virtualbox" do |vb|
      vb.cpus = 1
      vb.name = "nginxn1"
      vb.memory = "512"
    end
    
      nginxn1.vm.provision "shell", path: "#{current_dir}/provision/nginx-nodes/provision-nginx-nodes.sh",
      env:  {
          "PHPVERSION"			=> "#{configsettings["php_v"]}",
          "DBNAME"			=> "#{configsettings["database_name"]}",
          "DBUSER"			=> "#{configsettings["database_user"]}",
          "DBPASSWORD"			=> "#{configsettings["database_password"]}",
          "SITEPATH"			=> "#{configsettings["site_path_directory"]}",
          "DBSERVER"			=> "#{configsettings["site_db_server"]}"
      }
  end
  
  config.vm.define "nginx-node2" do |nginxn2|
    nginxn2.vm.hostname = "nginxn2"
    nginxn2.vm.network "private_network", ip: "172.16.0.11"
    
    nginxn2.vm.provider "virtualbox" do |vb|
      vb.cpus = 1
      vb.name = "nginxn2"
      vb.memory = "512"
    end
    
    nginxn2.vm.provision "shell", path: "#{current_dir}/provision/nginx-nodes/provision-nginx-nodes.sh"
  end
  
  
  config.vm.define "nginx-node3" do |nginxn3|
    nginxn3.vm.hostname = "nginxn3"
    nginxn3.vm.network "private_network", ip: "172.16.0.12"
    
    nginxn3.vm.provider "virtualbox" do |vb|
      vb.cpus = 1
      vb.name = "nginxn3"
      vb.memory = "512"
    end
    
    nginxn3.vm.provision "shell", path: "#{current_dir}/provision/nginx-nodes/provision-nginx-nodes.sh"
  end
  
  
  config.vm.define "nginx-upstream" do |nginxup|
    nginxup.vm.hostname = "nginx-upstream"
    nginxup.vm.network "private_network", ip: "172.16.0.14"
    nginxup.vm.network "public_network", ip: "192.168.0.110"
    
    nginxup.vm.provider "virtualbox" do |vb|
      vb.cpus = 1
      vb.name = "nginx-upstream"
      vb.memory = "512"
    end
    
    nginxup.vm.provision "shell", path: "#{current_dir}/provision/nginx-upstream/provision-nginx-master.sh"
  end
  
  
  config.vm.define "mysql" do |db|
    db.vm.hostname = "mysql.db"
    db.vm.network "private_network", ip: "172.16.0.13"
    db.vm.network "forwarded_port", guest: 22, host: 2223, id: "ssh"
    db.vm.network "forwarded_port", guest: 3306, host: 3306, auto_correct: true
    
    db.vm.provider "virtualbox" do |vb|
      vb.name = "db"
      vb.memory = "1024"
      vb.cpus = 1
    end
    
      db.vm.provision "shell", path: "#{current_dir}/provision/mysql/provision-mysql.sh",
      env:  {
          "DBNAME"			=> "#{configsettings["database_name"]}",
          "DB_USER"			=> "#{configsettings["database_user"]}",
          "DBPASSWD"			=> "#{configsettings["database_password"]}",
          "ROOT_DBPASSWD"		=> "#{configsettings["database_root_db_password"]}"
      }
  end
  
end
