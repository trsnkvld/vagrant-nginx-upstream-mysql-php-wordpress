#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

echo -e "\e[33m Устанавливаем MySQL root user password \e[0m"
debconf-set-selections <<< "mysql-server mysql-server/root_password password $ROOT_DBPASSWD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $ROOT_DBPASSWD"

echo -e "\e[33m Устанавливаем MySQL server \e[0m"
apt-get -y install mysql-server mysql-client > /dev/null

echo -e "\e[33m Создаём файл конфигурации учетной записи \e[0m"
cat > /root/.my.cnf <<EOF
[client]
user="root"
password="$ROOT_DBPASSWD"
EOF

echo -e "\e[33m Создаём базу данных сервера разработки приложений \e[0m"
sudo mysql -uroot -e "CREATE DATABASE IF NOT EXISTS $DBNAME;"
sudo mysql -uroot -e "CREATE USER '$DB_USER'@'%' IDENTIFIED BY '$DBPASSWD';"
sudo mysql -uroot -e "GRANT ALL PRIVILEGES ON $DBNAME.* TO '$DB_USER'@'%'"
#sudo mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION"
sudo mysql -uroot -e "FLUSH PRIVILEGES;"
echo -e "\e[33m Перезагружаем mysql... \e[0m"
sudo service mysql restart

echo -e "\e[33m Разрешаем доступ извне... \e[0m"
sudo sed -i "s/.*bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf

echo -e "\e[33m Перезагружаем mysql... \e[0m"
sudo service mysql restart
