#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

echo -e "\e[33m Устанавливаем Nginx \e[0m"
sudo apt-get update
sudo apt-get -y install nginx > /dev/null

echo -e "\e[33m Создаём файл конфигурации nginx \e[0m"
cat > /etc/nginx/sites-available/default <<EOF
log_format upstreamlog '\$server_name to: \$upstream_addr [\$request] '
	'upstream_response_time \$upstream_response_time '
	'msec \$msec request_time \$request_time';

upstream notes {
	server 172.16.0.10;
	server 172.16.0.11;
	server 172.16.0.12;
}

server {
	listen 80 default_server;
	listen [::]:80 default_server;

	root /var/www/html;

	#index index.html index.htm index.nginx-debian.html;

	server_name wordpress.active;

	access_log /var/log/nginx/access.log upstreamlog;

	location / {
		proxy_pass http://notes;
		try_files \$uri \$uri/ =404;
	}
}
EOF

sudo service nginx restart
