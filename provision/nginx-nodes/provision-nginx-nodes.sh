#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

echo -e "\e[33m Updating apt sources... \e[0m"
sudo apt-get update -qq > /dev/null

echo -e "\e[33m Installing PHP... \e[0m"
sudo apt -y install -qq php$PHPVERSION-fpm php$PHPVERSION-curl php$PHPVERSION-gd php$PHPVERSION-intl php$PHPVERSION-mbstring php$PHPVERSION-soap php$PHPVERSION-xml php$PHPVERSION-xmlrpc php$PHPVERSION-zip php$PHPVERSION-mysql > /dev/null
sudo service php$PHPVERSION-fpm restart

echo -e "\e[33m Installing NGinx and removing default config... \e[0m"
sudo apt-get -y install -qq nginx > /dev/null

# Remove the default nginx config from sites-enabled
#sudo rm /etc/nginx/sites-enabled/default

echo -e "\e[33m Setting up NGinx for Wordpress... \e[0m"

cat > /etc/nginx/sites-enabled/default << EOF
server {
        server_name _;
        root /var/www/html; # путь к WP
        index index.php;
        gzip on; # включаем сжатие gzip
        gzip_disable "msie6";
        gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/javascript;
        location ~ /\. {
                deny all; # запрет для скрытых файлов
        }
        #location ~* /(?:uploads|files)/.*\.php$ {
        #        deny all; # запрет для загруженных скриптов
        #}
        location ~* ^.+\.(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|rss|atom|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)$ {
                access_log off;
                log_not_found off;
                expires max; # кеширование статики
        }
        location / {
                try_files \$uri \$uri/ /index.php\$query_string;
        }
        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
        }
}

EOF


echo -e "\e[33m Downloading and installing Wordpress and dependencies... \e[0m"
wget https://wordpress.org/latest.tar.gz -q -P /tmp > /dev/null
tar xzfC /tmp/latest.tar.gz /tmp
rm /tmp/latest.tar.gz


echo -e "\e[33m Setting up Wordpress configurations and file permissions... \e[0m"

cp /tmp/wordpress/wp-config-sample.php  /tmp/wordpress/wp-config.php
sed -i "s/database_name_here/$DBNAME/"  /tmp/wordpress/wp-config.php
sed -i "s/username_here/$DBUSER/"       /tmp/wordpress/wp-config.php
sed -i "s/password_here/$DBPASSWORD/"   /tmp/wordpress/wp-config.php
sed -i "s/localhost/$DBSERVER/"  	/tmp/wordpress/wp-config.php

# Add authentication salts from the Wordpress API
SALT=$(curl -L https://api.wordpress.org/secret-key/1.1/salt/)
STRING='put your unique phrase here'
printf '%s\n' "g/$STRING/d" a "$SALT" . w | ed -s /tmp/wordpress/wp-config.php

sudo rsync -aqP /tmp/wordpress/ $SITEPATH/
sudo chown -R www-data:www-data $SITEPATH/
rm -r /tmp/wordpress
sudo rm /var/www/html/index.nginx-debian.html > /dev/null

# Setup file and directory permissions on Wordpress
sudo find $SITEPATH/ -type d -exec chmod 0755 {} \;
sudo find $SITEPATH/ -type f -exec chmod 0644 {} \;
sudo chmod 0640 $SITEPATH/wp-config.php

sudo service nginx restart
sudo service php$PHPVERSION-fpm restart


